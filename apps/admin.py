from django.contrib import admin

from .models import Article
from .models import Category


class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'updated', 'created', 'author']
    list_filter = ['created', 'updated', 'author']
    search_fields = ['title', 'author']

    class Meta:
        model = Article


admin.site.register(Article, ArticleAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']

    class Meta:
        model = Category


admin.site.register(Category, CategoryAdmin)
