from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

from .forms import ArticleForm, UserSignUpForm, EditProfileForm, PassChangeForm
from .models import Article


def index_page(request):
    queryset = Article.objects.all()[:5]
    res = {
        "article_list": queryset
    }
    return render(request, 'blog/index.html', res)


@login_required
def create_article(request):
    form = ArticleForm(request.POST or None)

    if form.is_valid():
        instance = form.save(commit=False)
        instance.author = User.objects.get(username=request.user.username)
        instance.save()
        return HttpResponseRedirect(instance.get_absolut_url())

    res = {
        "form": form
    }
    return render(request, 'blog/create.html', res)


@login_required
def edit_article(request, pk=None):
    queryset = get_object_or_404(Article, pk=pk)

    if not request.user == queryset.author:
        return HttpResponseRedirect(queryset.get_absolut_url())

    form = ArticleForm(request.POST or None, instance=queryset)

    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return HttpResponseRedirect(instance.get_absolut_url())

    res = {
        "article": queryset,
        "form": form
    }
    return render(request, 'blog/create.html', res)


def view_article(request, pk=None):
    queryset = get_object_or_404(Article, pk=pk)
    res = {
        "article": queryset
    }
    return render(request, 'blog/view.html', res)


@login_required
def delete_article(request):
    pass


@login_required
def profile(request, user=None):
    userset = User.objects.get(username=user)
    queryset = Article.objects.filter(author__username=user)
    paginator = Paginator(queryset, 5)

    page = request.GET.get('page')
    article_list = paginator.get_page(page)
    res = {
        'user_set': userset,
        'articles': article_list
    }
    return render(request, 'blog/profile.html', res)


@login_required
def edit_profile(request, user=None):
    userset = User.objects.get(username=user)

    if not request.user.username == userset.username:
        return redirect('profile_page', user=user)

    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('profile_page', user=user)
    else:
        form = EditProfileForm(instance=request.user)

    res = {
        'form': form
    }

    return render(request, 'blog/edit_profile.html', res)


def sign_up(request):
    if request.method == 'POST':
        form = UserSignUpForm(request.POST)
        if form.is_valid():
            form.save()
            # update_session_auth_hash(request, form.user)
            return redirect('login')
    else:
        form = UserSignUpForm()

    res = {
        'form': form
    }

    return render(request, 'blog/sign_up.html', res)


@login_required
def change_password(request, user=None):
    userset = User.objects.get(username=user)

    if not request.user.username == userset.username:
        return redirect('profile_page', user=user)

    if request.method == 'POST':
        form = PassChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('profile_page', user=user)
    else:
        form = PassChangeForm(user=request.user)

    res = {
        'form': form
    }

    return render(request, 'blog/change_password.html', res)


def terms_of_use(request):
    return  render(request, 'blog/terms_of_use.html')


def get_article_by_category(request, category=None):

    if category == 'all':
        queryset = Article.objects.all()

    else:
        queryset = Article.objects.filter(category__name=category)

    if not queryset.__len__():
        return redirect('category', category='all')

    paginator = Paginator(queryset, 5)

    page = request.GET.get('page')
    article_list = paginator.get_page(page)

    res = {
        'articles': article_list
    }

    return render(request, 'blog/category.html', res)

