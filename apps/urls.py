from django.urls import path

from . import views

urlpatterns = [
    path('', views.index_page, name="start_page"),
    path('create/', views.create_article, name="create_article"),
    path('edit/<int:pk>/', views.edit_article, name="edit_article"),
    path('view/<int:pk>/', views.view_article, name="view_article"),
    path('delete/', views.delete_article, name="delete_article"),
    path('profile/<str:user>/', views.profile, name="profile_page"),
    path('profile/<str:user>/edit/', views.edit_profile, name="edit_profile_page"),
    path('profile/<str:user>/password/', views.change_password, name="edit_password_page"),
    path('sign_up/', views.sign_up, name="sign_up"),
    path('terms/', views.terms_of_use, name="terms_of_use"),
    path('category/<str:category>', views.get_article_by_category, name="category")
]
