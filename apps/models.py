from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # photo = models.ImageField()
    age = models.IntegerField()
    role = models.CharField(max_length=20)


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Article(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=120, blank=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=False)
    text = models.TextField(blank=False)
    points = models.IntegerField(default=0)
    withAuth = models.BooleanField(default=False)
    adultContent = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def get_absolut_url(self):
        return "/view/%s" % self.pk

    class Meta:
        ordering = ['-updated', '-created']

class Bookmark(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    article = models.ForeignKey(Article, on_delete=models.CASCADE, blank=False)