from django import template
from datetime import datetime, timedelta
import pytz

register = template.Library()

utc = pytz.UTC


@register.filter(name='hot')
def hot(value):
    return value > utc.localize(datetime.now() - timedelta(days=1))
